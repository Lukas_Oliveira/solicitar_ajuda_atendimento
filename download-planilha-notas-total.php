<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
	
<?php 
require_once("logica-usuario.php");
function carregaClasse($nomeDaClasse) {
    require_once("class/".$nomeDaClasse.".php");
}

spl_autoload_register("carregaClasse");
require_once("conecta.php");
verificaUsuario2();
 $usuarioDao = new UsuarioDao($conexao);	

$usuarios = $usuarioDao->listaAtendentes();
$notaMonitoriaFactory = new NotaMonitoriaFactory();


	$monitoriaDao = new MonitoriaDao($conexao);

	$notaMonitoriaFactory = new notaMonitoriaFactory();
	$notaMonitoriaDao = new notaMonitoriaDao($conexao);

	$notaMonitoria = $notaMonitoriaFactory->geraMediaMonitoriasGerais($conexao);

	$script =  $notaMonitoria->getScript();
	$ligacao = $notaMonitoria->getLigacao();
	$cadastro = $notaMonitoria->getCadastro();
	$procedimento = $notaMonitoria->getProcedimento();
	$pesquisa = $notaMonitoria->getPesquisa();
	$total = $notaMonitoria->getTotal();
	$quantidade = $monitoriaDao->ContaTotalMonitoria();

	$arquivo = 'Relatório geral.xls';
	$html = '';
	$html .= '<table border="1">';
	$html .= '<tr>';
	$html .= '<td colspan="9" align="center"> Média geral das monitorias </tr>';
	$html .= '</tr>';

	$html .= '<tr></tr>';

	$html .= '<tr>';
	$html .= '<td colspan="9" align="center">Média geral </tr>';
	$html .= '</tr>';
	$html .= '<td></td>';
	$html .= '<td align="center">Quantidade</td>';
	$html .= '<td align="center">Script</td>';
	$html .= '<td align="center">Ligação</td>';
	$html .= '<td align="center">Cadastro</td>';
	$html .= '<td align="center">Procedimento</td>';
	$html .= '<td align="center">Pesquisa</td>';
	$html .= '<td align="center">Total</td>';
	$html .= '</tr>';
	$html .= '<tr>';
	$html .= '<td></td>';
	$html .= '<td align="center">'.number_format($quantidade[0]).'</td>';
	$html .= '<td align="center">'.number_format($script).'</td>';
	$html .= '<td align="center">'.number_format($ligacao).'</td>';
	$html .= '<td align="center">'.number_format($cadastro).'</td>';
	$html .= '<td align="center">'.number_format($procedimento).'</td>';
	$html .= '<td align="center">'.number_format($pesquisa).'</td>';
	$html .= '<td align="center">'.number_format($total).'</td>';
	$html .= '</tr>';
	$html .= '<tr></tr>';


	$html .= '<tr>';
	$html .= '<td colspan="9" align="center">Médias Por Atendente: </tr>';
	$html .= '</tr>';
	$html .= '<tr>';
	$html .= '<td align="center">ID</td>';
	$html .= '<td align="center">Atendente</td>';
	$html .= '<td align="center">Script</td>';
	$html .= '<td align="center">Ligação</td>';
	$html .= '<td align="center">Cadastro</td>';
	$html .= '<td align="center">Procedimento</td>';
	$html .= '<td align="center">Pesquisa</td>';
	$html .= '<td align="center">Total</td>';
	$html .= '</tr>';
	foreach($usuarios as $usuario) :
	$notaMonitoria = $notaMonitoriaFactory->geraMediaMonitorias($usuario->getId(), $conexao);
	
			if(null !== $notaMonitoria->getTotal()){
			// Criamos uma tabela HTML com o formato da planilha
			$html .= '<tr>';
			$html .= '<td align="center">'.$usuario->getId().'</td>';
			$html .= '<td align="center">'.substr($usuario->getNome(), 0, 40).'</td>';
			$html .= '<td align="center">'.substr($notaMonitoria->getScript(),0,4).'</td>';
			$html .= '<td align="center">'.substr($notaMonitoria->getLigacao(),0,4).'</td>';
			$html .= '<td align="center">'.substr($notaMonitoria->getCadastro(),0,4).'</td>';
			$html .= '<td align="center">'.substr($notaMonitoria->getProcedimento(),0,4).'</td>';
			$html .= '<td align="center">'.substr($notaMonitoria->getPesquisa(),0,4).'</td>';
			$html .= '<td align="center">'.substr($notaMonitoria->getTotal(),0,4).'</td>';
		}else {
			$html .= '<tr>';
			$html .= '<td align="center">'.$usuario->getId().'</td>';
			$html .= '<td align="center">'.substr($usuario->getNome(), 0, 13).'</td>';
			$html .= '<td align="center">--</td>';
			$html .= '<td align="center">--</td>';
			$html .= '<td align="center">--</td>';
			$html .= '<td align="center">--</td>';
		}

	endforeach;
	$html .= '</table>';
 //require_once("footer.php");
header('Content-Type: application/vnd.ms-excel');
    header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );

    // Se for o IE9, isso talvez seja necessário
    header('Cache-Control: max-age=1');

header ("Content-Description: PHP Generated Data" );
echo $html;
exit; ?>
</body>
</html>

