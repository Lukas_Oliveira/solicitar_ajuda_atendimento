<?php
	class ChamadoFactory {
		public function criaChamado($params){
			//print_r($_POST);
			//print_r($_SESSION);
			//print($params['nomeAutor']);
			$autor = $params['autor'];
			$nomeAutor = $params['nomeAutor'];
			$emailAutor = $params['emailAutor'];
			$areaAutor = $params['areaAutor'];
			$data = $params['data'];
			$hora = $params['hora'];
			$motivo = $params['motivo'];
			$situacao = $params['situacao'];
			$tipo = $params['tipo'];

			return new Chamado($autor, $nomeAutor, $emailAutor, $areaAutor, $data, $hora, $motivo,$situacao,$tipo);
		}
		public function criaChamadoLista($id, $autor, $nomeAutor, $emailAutor, $areaAutor, $data, $hora, $motivo,$situacao,$tipo){
			$chamado = new Chamado($autor, $nomeAutor, $emailAutor, $areaAutor, $data, $hora, $motivo,$situacao,$tipo);
			$chamado->setId($id);

			return $chamado;
		}
	}
?>