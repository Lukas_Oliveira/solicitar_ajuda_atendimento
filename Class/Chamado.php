<?php
	class Chamado{
		private $id;
		private $autor;
		private $nomeAutor;		
		private $emailAutor;
		private $areaAutor;
		private $data;
		private $hora;
		private $dataSolucao;
		private $horaSolucao;
		private $motivo;
		private $situacao;
		private $tipo;

		function __construct($autor, $nomeAutor,$emailAutor,$areaAutor,$data,$hora,$motivo,$situacao,$tipo){
			$this ->autor = $autor;
			$this ->nomeAutor = $nomeAutor;
			$this ->emailAutor = $emailAutor;
			$this ->areaAutor = $areaAutor;
			$this ->data = $data;
			$this ->hora = $hora;
			$this ->motivo = $motivo;
			$this ->situacao = $situacao;
			$this ->tipo = $tipo;
		}

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getAutor(){
		return $this->autor;
	}

	public function setAutor($autor){
		$this->autor = $autor;
	}

	public function getNomeAutor(){
		return $this->nomeAutor;
	}

	public function setNomeAutor($nomeAutor){
		$this->nomeAutor = $nomeAutor;
	}

	public function getEmailAutor(){
		return $this->emailAutor;
	}

	public function setEmailAutor($emailAutor){
		$this->EmailAutor = $emailAutor;
	}
		public function getAreaAutor(){
		return $this->areaAutor;
	}

	public function setAreaAutor($areaAutor){
		$this->areaAutor = $areaAutor;
	}

	public function getData(){
		return $this->data;
	}

	public function setData($data){
		$this->data = $data;
	}

	public function getHora(){
		return $this->hora;
	}

	public function setHora($hora){
		$this->hora = $hora;
	}
	public function getDataSolucao(){
		return $this->dataSolucao;
	}

	public function setDataSolucao($dataSolucao){
		$this->dataSolucao = $dataSolucao;
	}

	public function getHoraSolucao(){
		return $this->horaSolucao;
	}

	public function setHoraSolucao($horaSolucao){
		$this->horaSolucao = $horaSolucao;
	}

	public function getMotivo(){
		return $this->motivo;
	}

	public function setMotivo($motivo){
		$this->motivo = $motivo;
	}

	public function getSituacao(){
		return $this->situacao;
	}

	public function setSituacao($situacao){
		$this->situacao = $situacao;
	}
	public function getTipo(){
		return $this->tipo;
	}

	public function setTipo($tipo){
		$this->tipo = $tipo;
	}
}
?>