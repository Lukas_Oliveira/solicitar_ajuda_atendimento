<?php
	class DaoLoader {
		private $conexao;

		function __construct($conexao){
			$this->conexao = $conexao;
		}

		function loadChamadoDao(){
			$chamadoDao = new ChamadoDao($this->conexao);
			return $chamadoDao;
		}

		function loadNotaDao(){
			$notaDao = new NotaDao($this->conexao);
			return $notaDao;
		}

		function loadSolicitacaoDao(){
			$solicitacaoDao = new SolicitacaoDao($this->conexao);
			return $solicitacaoDao;
		}
		function loadUsuarioDao(){
			$usuarioDao = new UsuarioDao($this->conexao);
			return $usuarioDao;
		}
	}
?>