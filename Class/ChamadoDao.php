<?php
	class ChamadoDao {
		private $conexao;

		function __construct($conexao){
			$this->conexao = $conexao;
		}
		function criaChamado(Chamado $chamado){
			$query = "INSERT INTO `chamado`(`autor`, `nomeAutor`, `EmailAutor`, `areaAutor`, `data`, `hora`, `motivo`, `situacao`, `tipo`) VALUES ('{$chamado->getAutor()}','{$chamado->getNomeAutor()}','{$chamado->getEmailAutor()}','{$chamado->getAreaAutor()}','{$chamado->getData()}','{$chamado->getHora()}','{$chamado->getMotivo()}','{$chamado->getSituacao()}','{$chamado->getTipo()}')";
			return mysqli_query($this->conexao, $query);
		}
		function listaChamadosCliente($idAutor){
			$chamadoFactory = new ChamadoFactory();
			$chamados = array();
			$query = mysqli_query($this->conexao, "SELECT `id`, `autor`, `nomeAutor`, `EmailAutor`, `areaAutor`, `data`, `hora`, `motivo`, `situacao`, `tipo` FROM `chamado` WHERE  autor = '{$idAutor}'");
			while($chamados_array = mysqli_fetch_assoc($query)) {
				$chamado = $chamadoFactory->criaChamadoLista($chamados_array['id'],$chamados_array['autor'],$chamados_array['nomeAutor'],$chamados_array['EmailAutor'],$chamados_array['areaAutor'],$chamados_array['data'],$chamados_array['hora'],$chamados_array['motivo'],$chamados_array['situacao'],$chamados_array['tipo']);

				array_push($chamados, $chamado);
			}
			return $chamados;
		}
		function listaChamadosTratar(){
			$chamadoFactory = new ChamadoFactory();
			$chamados = array();
			$query = mysqli_query($this->conexao, "SELECT `id`, `autor`, `nomeAutor`, `EmailAutor`, `areaAutor`, `data`, `hora`, `motivo`, `situacao`, `tipo` FROM `chamado`");
			while($chamados_array = mysqli_fetch_assoc($query)) {
				$chamado = $chamadoFactory->criaChamadoLista($chamados_array['id'],$chamados_array['autor'],$chamados_array['nomeAutor'],$chamados_array['EmailAutor'],$chamados_array['areaAutor'],$chamados_array['data'],$chamados_array['hora'],$chamados_array['motivo'],$chamados_array['situacao'],$chamados_array['tipo']);

				array_push($chamados, $chamado);
			}
			return $chamados;
		}
		function buscaChamado($id){
			$resultado = mysqli_query($this->conexao,"SELECT `id`, `autor`, `nomeAutor`, `EmailAutor`, `areaAutor`, `data`, `hora`, `motivo`, `situacao`, `tipo` FROM `chamado` WHERE id = '{$id}';");

			$params = mysqli_fetch_assoc($resultado);

				$chamadoFactory = new ChamadoFactory();
				$chamado = $chamadoFactory->criaChamadoLista($params['id'],$params['autor'],$params['nomeAutor'],$params['EmailAutor'],$params['areaAutor'],$params['data'],$params['hora'],$params['motivo'],$params['situacao'],$params['tipo']);
				
		return $chamado;
		}
		function solucionar($id,$dataSolucao,$horaSolucao){
			$query = "UPDATE `chamado` SET `situacao`='2',`dataSolucao`='{$dataSolucao}',`horaSolucao`='{$horaSolucao}' WHERE `id` ='{$id}'";
			return mysqli_query($this->conexao, $query);;
		}

	}
?>
