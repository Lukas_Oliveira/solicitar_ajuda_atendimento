<?php 
require_once("menu.php");
 ?>
<table class="table table-striped table-bordered">
<?php
	$usuarioDao = new UsuarioDao($conexao);
	$usuarios = $usuarioDao->listaUsuarios();
	?>
	<tr>
	<td class="text-success">Nome</td>
	<td class="text-success">E-mail</td>
	<td class="text-success">Área</td>	
	<td class="text-success">Administrador</td>	
	<td class="text-success">Atualizar</td>	
	<td class="text-success">Desativar</td>
	</tr>
	<?php	
	foreach($usuarios as $usuario) :
	?>
		<form action="altera-usuario-formulario.php" method="post">
		<tr>
			<td><?=substr($usuario->getNome(), 0, 40)  ?></td>
			<td><?= $usuario->getEmail() ?></td>
			<td><?= $usuario->getArea() ?></td>
			 <td><?php switch ($usuario->getAdm()) {
				case '0':
					?> <h3 class="lead">Não </h3><?php
					break;
				case '1':
					?><h3 class="lead text-success">Sim </h3><?php
					break;
			} ?></td>
			</td>
			<input class="form-control" type="hidden" name="id" value="<?=$usuario->getId()?>" required>
			<td>
				<button class="btn btn-primary" type="submit">Atualizar</button>
			</td>
			</form>
			<form action="desativar-usuario.php" method="post">
			<input class="form-control" type="hidden" name="id" value="<?=$usuario->getId()?>" required>
			<td>
				<!--<button class="btn btn-danger" type="submit" onclick="return confirm('Realmente deseja deletar este usuário? Todos os dados do usuário serão apagados, Esta ação não pode ser revertida');">Desativar</button>-->
				<button class="btn btn-danger" type="submit" onclick="return confirm('Realmente deseja desativar este usuário? Suas informações estarão indisponíveis enquanto desativado.');">Desativar</button>
			</td>
		</tr>
	</form>
	<?php
	endforeach
	?>	
</table>
    	</div>
   		 </div>
	</div>
	</body>
</html>
<?php // require_once("footer.php"); ?>