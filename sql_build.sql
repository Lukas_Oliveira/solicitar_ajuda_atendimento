-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema n1help
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema n1help
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `n1help` DEFAULT CHARACTER SET utf8 ;
USE `n1help` ;

-- -----------------------------------------------------
-- Table `n1help`.`atendente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `n1help`.`atendente` (
  `id_atendente` INT NOT NULL,
  `nome_atendente` VARCHAR(100) NULL,
  PRIMARY KEY (`id_atendente`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `n1help`.`mvno`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `n1help`.`mvno` (
  `id_mvno` INT NOT NULL,
  `mvnocol` VARCHAR(45) NULL,
  PRIMARY KEY (`id_mvno`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `n1help`.`solicitacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `n1help`.`solicitacao` (
  `id_solicitacao` INT NOT NULL,
  `time_inicio_solicitacao` DATETIME NULL,
  `time_final_solicitacao` DATETIME NULL,
  `id_atendente` INT NOT NULL,
  `id_mvno` INT NOT NULL,
  PRIMARY KEY (`id_solicitacao`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
