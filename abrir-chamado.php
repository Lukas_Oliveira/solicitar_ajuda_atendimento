<?php
require_once("menu.php");
require_once("email-adm.php");
$chamadoFactory = new ChamadoFactory();
$chamadoDao = new chamadoDao($conexao);
$chamado = $chamadoFactory->criaChamado($_POST);

if($chamadoDao->criaChamado($chamado)){
	enviaAbertura();
	notificaAberturaDeChamado($chamado->getNomeAutor(),$chamado->getEmailAutor(),$chamado->getAreaAutor(),$chamado->getData(), $chamado->getHora(),$chamado->getMotivo(),$chamado->getSituacao(),$chamado->getTipo());
	?>
	<h2 class="lead"><p class="text-success">Chamado registrado com succeso.</p></h2>
	<?php

}else{
	?>
	<h2><p class="text-danger">Erro no registro. Verifique os campos e tente novamente </p></h2>
	<?php
}
require_once("abrir-chamado-formulario.php");
?>
<html>
	<body>