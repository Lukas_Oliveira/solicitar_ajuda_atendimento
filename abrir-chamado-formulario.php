﻿<html>
<head>
</head>
	<body>
	<?php 
	 require_once("logica-usuario.php");
	 verificaAcesso(); 
   require_once("menu.php");
	 $usuarioDao = new UsuarioDao($conexao);
   date_default_timezone_set('America/Sao_Paulo');
   $data = date("Y-m-d");
   $hora = date("H:i:s");
	  ?>	<div>
				<h1 class="lead">Novo chamado</h1>
			<form  id="abrir-chamado" action="abrir-chamado.php" method="post">
				<table class="table">
					<tr>
						<td>
              <label for="nomeAutor">Solicitante</label>
							<input type="text" name="nomeAutor" class="form-control" value="<?= $_SESSION["usuario_logado"]?>" id="nomeAutor" readonly>
						</td>
						<td>
              <label for="email">Email</label>
							<input type="text" name="emailAutor" class="form-control" value="<?= $_SESSION["email"]?>" readonly>
						</td>
            <td>
              <label for="areaAutor">Area de atuação</label>
              <input class="form-control" type="text" name="areaAutor" placeholder="Area" value="<?= $_SESSION["area"] ?>" required readonly>
            </td>
					</tr>
					<tr>
						<td>
              <label for="data">Data</label>
							<input class="form-control" type="data" name="data" placeholder="data" value="<?= $data?>" required readonly>
						</td>
						<td>
              <label for="hora">Hora</label>
							<input class="form-control" type="text" name="hora" placeholder="Hora"  value="<?= $hora ?>" required readonly>
						</td>
             <td>
              <label for="situacao">Situação do chamado</label>
              <input class="form-control" type="text" name="situacao" placeholder="Situacao" value="Aberto" required readonly>
            </td>
					</tr>
					<tr>
						<td>
               <label for="tipo">Atendimento</label>
              <select class="form-control" type="text" name="tipo" required>
                <option selected="true" disabled="disabled" value="">Selecione</option>
                <option value="- Configuração windows">- Configuração windows</option>
                <option value="- Office - Outlook">- Office - Outlook</option>
                <option value="- Office - Demais programas">- Office - Demais programas</option>
                <option value="- Impressoras e dispositivos">- Impressoras e dispositivos</option>
                <option value="- Programas externos">- Programas externos</option>
                <option value="- Outros">- Outras solicitações</option>
            </td>
          </table>
            <tr>
            <td>
              <label for="motivo">Informação de solicitação</label>
               <textarea form="abrir-chamado" class="form-control" rows="5" id="motivo" name="motivo" placeholder="Solicitação"></textarea> 
             </td>
            </tr>
						<table>
					<tr>
						<td>
             <?php
              date_default_timezone_set('America/Sao_Paulo');
               $dataRegistro = date('y-m-d', time());
                ?>
              <input class="form-control" type="hidden" name="autor" value="<?=$_SESSION["id"]?>" required>
            </td>
             <tr>
              <td>
              <button class="btn btn-primary" type="submit">Abrir chamado</button> 
              </tr>
            </td>
          </tr>
        </table>
			</form>
		</div>
	</body>
</html>
