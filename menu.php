<php?@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"?>
<html>
<?php
require_once("logica-usuario.php");
function carregaClasse($nomeDaClasse) {
    require_once("class/".$nomeDaClasse.".php");
}

spl_autoload_register("carregaClasse");
require_once("conecta.php");

 if (!isset($_SESSION)) {
    session_start();
}
?>

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

</head>

 <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->

<?php
if($_SESSION['nivel']=='0'){
    ?>

    <body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="home.php">
                            <img src="images/logo.png" alt="Cartorio" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-desktop"></i>Opções
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="home.php">Home</a>
                                </li>
                                <li>
                                    <a href="perfil-usuario.php">Perfil</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="home.php"><img src="images/logo.png" alt="Logo" /></a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li>
                            <a href="home.php">
                                <i class="zmdi zmdi-home"></i>Home
                            </a>
                        </li>
                        <li>
                            <a href="perfil-usuario.php">
                                <i class="zmdi zmdi-account"></i>Perfil
                            </a>
                        </li>
                        <li>
                            <a href="logout.php">
                                <i class="zmdi zmdi-power"></i>Logout
                            </a>
                        </li>
                        </ul>
                    </form>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
      <div class="page-container"> 
             <!-- HEADER DESKTOP-->
             <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <form class="form-header" action="" method="POST">
                            <h3 class="lead"> Seja Bem Vindo</h3>
                        </form>
                        
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <!--<img src="images/icon/avatar-00.jpg" alt="usuario" /> -->
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php  echo $_SESSION["usuario_logado"] ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <!--<img src="images/icon/avatar-01.jpg" alt="usuario" /> --->
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php  echo $_SESSION["usuario_logado"] ?></a>
                                                    </h5>
                                                    <span class="email"><?php  echo $_SESSION["email"] ?></span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="home.php">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="logout.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
             <!--END HEADER DESKTOP-->
        </div> 

    </div>

    <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->
    <!-------------------------------------------------------------------------------------------->

<?php
}else if($_SESSION['nivel']=='1'){
    ?>

    <body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="home.php">
                            <img src="images/logo.png" alt="" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-desktop"></i>Opções
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="home.php">Home</a>
                                </li>
                                <li>
                                    <a href="perfil-usuario.php">Perfil</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="home.php"><img src="images/logo.png" alt="Logo" /></a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li>
                            <a href="cadastra-monitoria-formulario.php">
                                <i class="fas fa-pencil-alt"></i>Nova Monitoria
                            </a>
                        </li>
                        <li>
                            <a href="home.php">
                                <i class="zmdi zmdi-home"></i>Home
                            </a>
                        </li>
                        <li>
                            <a href="perfil-usuario.php">
                                <i class="zmdi zmdi-account"></i>Perfil
                            </a>
                        </li>
                        <li>
                            <a href="logout.php">
                                <i class="zmdi zmdi-power"></i>Logout
                            </a>
                        </li>
                        </ul>
                    </form>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
      <div class="page-container"> 
             <!-- HEADER DESKTOP-->
             <header class="header-desktop">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="header-wrap">
                        <form class="form-header" action="" method="POST">
                            <h3 class="lead"> Seja Bem Vindo</h3>
                        </form>
                        
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <!--<img src="images/icon/avatar-00.jpg" alt="usuario" /> -->
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php  echo $_SESSION["usuario_logado"] ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <!--<img src="images/icon/avatar-01.jpg" alt="usuario" /> --->
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php  echo $_SESSION["usuario_logado"] ?></a>
                                                    </h5>
                                                    <span class="email"><?php  echo $_SESSION["email"] ?></span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="home.php">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="logout.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
             <!--END HEADER DESKTOP-->
        </div> 

    </div>
<?php
}else{
?>

<!-------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------
<!--    ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        ------------------------------------------------------------------
                            Monitoria Correios Celular
        ------------------------------------------------------------------
        Sistema desenvolvido por Lukas Barbosa de oliveira em - 20 10 2018
        ------------------------------------------------------------------
        Contato desenvolvedor: (11)951540387 - luka.oliveira4@hotmail.com
        ------------------------------------------------------------------

        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

        -------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->
<!-------------------------------------------------------------------------------------------->
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="home.php">
                            <img src="images/logo.png" alt="" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-desktop"></i>Opções
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="home.php">Home</a>
                                </li>
                            </ul>
                        </li>
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-folder-open"></i>Relatórios
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-chart-bar"></i>Desempenhos
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-cog"></i>opções
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
    	<aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="home.php"><img src="images/logo.png" alt="Logo" /></a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                    	<li>
                            <a href="cadastra-monitoria-formulario.php">
                                <i class="fas fa-pencil-alt"></i>Nova Monitoria
                            </a>
                        </li>
                         <li>
                            <a href="lista-usuarios.php">
                               <i class="far fa-address-book"></i>Lista de Usuarios
                            </a>
                        </li>
                        <li>
                            <a href="novo-usuario-formulario.php">
                               <i class="fas fa-user"></i>Novo usuario
                            </a>
                        </li>
                        <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-desktop"></i>Banco de horas
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="registra-horas-formulario.php">
                                        </i>Registrar dia
                                    </a>
                                </li>
                                     <li>
                            <a href="lista-dias-form.php">
                              Lista de dias
                            </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                        	<form action="controller.do" method="post">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-folder-open"></i>Relatórios
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="lista-atendentes.php">
                                        Lista de atendentes
                                     </a>
                                     <a href="relatorio-geral.php">
                                        Relatório Geral
                                     </a>
                                   <!-- <a href="lista-nota-monitoria-form.php">
                                        Lista de monitorias -->
                                     </a>
                                </li>
                            </ul>
                            </form>
                        </li>
                        <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-chart-bar"></i>Desempenhos
                            </a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                <li>
                                    <a href="lista-equipe-monitoria.php">
                                        Equipe monitoria
                                     </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="logout.php">
                                    <i class="zmdi zmdi-power"></i>Logout</a>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
    	<!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
             <!-- HEADER DESKTOP-->
             <header class="header-desktop">
        	<div class="section__content section__content--p30">
            	<div class="container-fluid">
                	<div class="header-wrap">
                		<form class="form-header" action="" method="POST">
                            <h3 class="lead"> Seja Bem Vindo</h3>
                        </form>
                    	
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <!--<img src="images/icon/avatar-00.jpg" alt="usuario" /> -->
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php  echo $_SESSION["usuario_logado"] ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                        <!--<img src="images/icon/avatar-01.jpg" alt="usuario" /> --->
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php  echo $_SESSION["usuario_logado"] ?></a>
                                                    </h5>
                                                    <span class="email"><?php  echo $_SESSION["email"] ?></span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="perfil-usuario.php">
                                                        <i class="zmdi zmdi-account"></i>Account</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Setting</a>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="logout.php">
                                                    <i class="zmdi zmdi-power"></i>Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
             <!--END HEADER DESKTOP-->
        </div>

    </div>
<?php
}
?>
        <script placeholder="Clicka aqui!!">
        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        ------------------------------------------------------------------
                            Monitoria Correios Celular
        ------------------------------------------------------------------
        Sistema desenvolvido por Lukas Barbosa de oliveira em - 20 10 2018
        ------------------------------------------------------------------
        Contato desenvolvedor: (11)951540387 - luka.oliveira4@hotmail.com
        ------------------------------------------------------------------

        ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    </script>
    <!-- Jquery JS-->
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="vendor/slick/slick.min.js">
    </script>
    <script src="vendor/wow/wow.min.js"></script>
    <script src="vendor/animsition/animsition.min.js"></script>
    <script src="vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="vendor/circle-progress/circle-progress.min.js"></script>
    <script src="vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="js/main.js"></script>

    <div class="page-wrapper"> 
    <!-- PAGE CONTAINER-->
        <div class="page-container">
        <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">


